#!/bin/bash

#list
alias l='ls' 					
alias ll='ls -lA'
alias la='ls -A'
alias ls='ls --color=auto'
alias l.="ls -A | egrep '^\.'"   

#fix typo
alias cd..='cd ..'
alias pdw='pwd'
alias vi='nvim'
alias vim='nvim'

# Colorize the grep command output for ease of use
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
	
# XBPS Package Manager
alias xup='sudo xbps-install -Su'		# Sync/Update (--sync, --update)
alias xsr='sudo xbps-query -Rs'			# Search package
alias xsp='sudo xbps-query -l | grep'	# Search Installed packages
alias xin='sudo xbps-install -S'    	# Install package
alias xun='sudo xbps-remove -R'   		# Remove package

# Clean-up
alias xuo='sudo xbps-remove -yo'   	# Remove Orphaned (Unused) Packages
alias rok='sudo vkpurge rm all'		# Remove old installed versions of the Linux kernel
alias xcc='sudo xbps-remove -yO'  	# Clean-Up Package Cache
    



