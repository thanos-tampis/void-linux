#################################
#
# Backend
#
#################################

# Backend to use: "xrender" or "glx".
# GLX backend is typically much faster but depends on a sane driver.

# backend = "xrender";
# backend = "xr_glx_hybrid";
backend = "glx";

#################################
#
# GLX backend
#
#################################

glx-no-stencil = true;

# GLX backend: Copy unmodified regions from front buffer instead of redrawing them all.
# My tests with nvidia-drivers show a 10% decrease in performance when the whole screen is modified,
# but a 20% increase when only 1/4 is.
# My tests on nouveau show terrible slowdown.
# Useful with --glx-swap-method, as well.
glx-copy-from-front = false;

# GLX backend: Use MESA_copy_sub_buffer to do partial screen update.
# My tests on nouveau shows a 200% performance boost when only 1/4 of the screen is updated.
# May break VSync and is not available on some drivers.
# Overrides --glx-copy-from-front.
# glx-use-copysubbuffermesa = true;
glx-use-copysubbuffermesa = false;

# GLX backend: Avoid rebinding pixmap on window damage.
# Probably could improve performance on rapid window content changes, but is known to break things on some drivers (LLVMpipe).
# Recommended if it works.
glx-no-rebind-pixmap = false;

# glx-swap-method has been deprecated since v6, your setting "undefined" should be replaced by `use-damage = true`
# glx-swap-method = "undefined";
use-damage = true;

#################################
#
# Shadows
#
#################################

shadow = true;

# The blur radius for shadows. (default 12)
shadow-radius = 7;

# The left offset for shadows. (default -15)
shadow-offset-x = -7;

# The top offset for shadows. (default -15)
shadow-offset-y = -7;

# The translucency for shadows. (default .75)
shadow-opacity = 0.6;

#################################
#
# Fading
#
#################################

fading = false;
fade-delta = 5.0;
fade-in-step = 0.03;
fade-out-step = 0.03;

#################################
#
# OTHER CONFIG
#
#################################

mark-wmwin-focused = true;
use-ewmh-active-win = true;
mark-ovredir-focused = true;
detect-rounded-corners = true;
detect-client-opacity = true;
#unredir-if-possible = true;

vsync = false;

opacity-rule = [
  # hack for i3 tabbed mode with semi-opaque windows being visible below the current tab
    "0:_NET_WM_STATE@:32a * = '_NET_WM_STATE_HIDDEN'"
];

shadow-exclude = [
    "! name~=''",
    "name = 'Notification'",
    "name = 'Docky'",
    "name = 'Plank'",    
    "name = 'Kupfer'",
    "name = 'xfce4-notifyd'",
    "name *= 'VLC'",
    "name *= 'compton'",
    "name *= 'picom'",
    "name *= 'Chromium'",
    "name *= 'Chrome'",
    "name *= 'subl'",    
    "name *= 'vscode'",
    "name *= 'code-oss'",

    # Exclude some special popup menu shadows, but Modal Windows.
    "class_g = 'Firefox' && argb",
    "class_g = 'firefox' && (window_type = 'utility' || window_type = 'popup_menu') && argb",
    "class_g = 'Firefox' && (window_type = 'utility' || window_type = 'popup_menu') && argb",
    "class_g = 'firefox-esr' && (window_type = 'utility' || window_type = 'popup_menu') && argb",
    "class_g = 'Firefox-esr' && (window_type = 'utility' || window_type = 'popup_menu') && argb",    
    "class_g = 'Conky'",
    "class_g = 'Kupfer'",
    "class_g = 'Synapse'",
    "class_g ?= 'Notify-osd'",
    "class_g ?= 'Cairo-dock'",
    "class_g ?= 'Xfce4-notifyd'",
    "class_g ?= 'Xfce4-power-manager'",
    "_GTK_FRAME_EXTENTS@:c",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
];

wintypes:
{
  tooltip = { fade = true; shadow = true; focus = true; };
  dock = { shadow = false; }
  dnd = { shadow = false; }
};

# Ref: https://github.com/jEsuSdA/the-perfect-desktop/blob/master/compton-picom/picom.conf