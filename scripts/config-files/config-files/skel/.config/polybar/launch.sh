#!/usr/bin/env sh

# More info : https://github.com/jaagr/polybar/wiki

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

desktop=$(echo $DESKTOP_SESSION)

case $desktop in
    dwm)
        if type "xrandr" > /dev/null; then
            for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
                MONITOR=$m polybar --reload dwm-bar -c ~/.config/polybar/config &
            done
        else
            polybar --reload dwm-bar -c ~/.config/polybar/config &
        fi
        ;;
    openbox)
        if type "xrandr" > /dev/null; then
            for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
                MONITOR=$m polybar --reload openbox-bar -c ~/.config/polybar/config &
            done
        else
            polybar --reload openbox-bar -c ~/.config/polybar/config &
        fi
        ;;
esac
