;=====================================================
;     Modules
;=====================================================


;;;;;;;;;;;;;;;
;;   rofi  ;;
;;;;;;;;;;;;;;;
[module/rofi]
type = custom/script
label = %output%
label-padding = 2
interval = 10
exec = echo ""
click-left = "rofi -combi-modi drun,window,ssh -theme solarized -width 40 -font 'Roboto 11' -show combi"
click-right = "rofi -modi run,drun,window -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'Noto Sans 10' "


;;;;;;;;;;;;;;;;;;;;;;;;
;; Openbox Workspaces ;;
;;;;;;;;;;;;;;;;;;;;;;;;
[module/workspaces]
type = internal/xworkspaces
pin-workspaces = false
enable-click = true
enable-scroll = true
format-padding = 0
icon-0 = 1;
icon-1 = 2;
icon-2 = 3;
icon-3 = 4;
icon-4 = 5;
icon-5 = 6;
icon-6 = 7;
icon-default =
format = <label-state>
label-active = %icon% %name%
label-occupied = %icon% %name%
label-urgent = %icon% %name%
label-empty = %icon% %name%
label-occupied-padding = 1
label-empty-padding = 1
label-urgent-padding = 1
label-active-padding = 1
label-active-foreground = ${colors.blue}
label-active-underline = ${colors.blue}
label-occupied-underline = ${colors.moderate}
label-urgent-foreground = ${colors.urgent}
label-urgent-underline = ${colors.urgent}
label-empty-foreground = ${colors.foreground}


;;;;;;;;;;;;;;;;;;;
;; DWM Workspaces ;;
;;;;;;;;;;;;;;;;;;;
[module/dwm]
type = internal/dwm
pin-workspaces = true
strip-wsnumbers = true
index-sort = true
enable-click = true
fuzzy-match = true
ws-icon-default = 
format = <label-state><label-mode>
label-mode = %mode%
label-mode-padding = 2
label-focused = %index% %name%
label-focused-padding = 2
label-unfocused = %index% %name%
label-unfocused-padding = 2
label-visible = %index% %name%
label-visible-padding = 2
label-urgent = %index% %name%
label-urgent-padding = 2
label-mode-underline = ${colors.urgent}
label-focused-underline = ${colors.blue}
label-visible-underline = ${colors.purple}
label-urgent-underline = ${colors.urgent}


;;;;;;;;;;;;;;;;;;;
;; Window Switch ;;
;;;;;;;;;;;;;;;;;;;
[module/window_switch]
type = custom/script
interval = 5
label = %output%
click-left = skippy-xd
click-right = skippy-xd
exec = echo "  "
format = <label>


;;;;;;;;;
;; Cpu ;;
;;;;;;;;;
[module/coreuse]
type = internal/cpu
interval = 3
format-padding = 1
format = <label> <ramp-coreload>
label = %{A1:$TERMINAL -e sh -c "htop -s PERCENT_CPU; bash" & disown:} %percentage:2%% %{A}
ramp-coreload-0 = ▂
ramp-coreload-1 = ▃
ramp-coreload-2 = ▄
ramp-coreload-3 = ▅
ramp-coreload-4 = ▆
ramp-coreload-5 = ▇
ramp-coreload-0-foreground = ${colors.blue}
ramp-coreload-1-foreground = ${colors.blue}
ramp-coreload-2-foreground = ${colors.moderate}
ramp-coreload-3-foreground = ${colors.moderate}
ramp-coreload-4-foreground = ${colors.urgent}
ramp-coreload-5-foreground = ${colors.urgent}


;;;;;;;;;;;;
;; Memory ;;
;;;;;;;;;;;;
[module/memory]
type = internal/memory
interval = 3
format = <label>
label-padding = 1
label = %{A1:$TERMINAL -e sh -c "htop -s PERCENT_MEM; bash" & disown:} %percentage_used%%%{A}


;;;;;;;;;;;
;; Clock ;;
;;;;;;;;;;;
[module/clock]
type = internal/date
format = <label>
interval = 5
time = %H:%M - %A %d %B
label = %{A1:gsimplecal & disown:} %{A3:gsimplecal & disown:} %time%%{A} %{A}


;;;;;;;;;;;;;;;;;
;; Temperature ;;
;;;;;;;;;;;;;;;;;
[module/temperature]
type = internal/temperature
interval = 3
thermal-zone = 0
warn-temperature = 70
format = <ramp><label>
format-padding = 1
label = %{A1:$TERMINAL -e sh -c "watch sensors; bash" & disown:} %temperature-c%%{A}
ramp-0 = ""
ramp-1 = ""
ramp-2 = ""
ramp-3 = ""
ramp-4 = ""
ramp-0-foreground = ${colors.blue}
ramp-1-foreground = ${colors.blue}
ramp-2-foreground = ${colors.moderate}
ramp-3-foreground = ${colors.moderate}
ramp-4-foreground = ${colors.urgent}
format-warn = <label-warn>
label-warn = "  %temperature-c%"
label-warn-padding = 1
label-warn-foreground = ${colors.urgent}


;;;;;;;;;;;;
;; Volume ;;
;;;;;;;;;;;;
[module/volume]
type = internal/alsa
master-mixer = Master
format-volume-padding = 1
label-volume-foreground = ${colors.foreground}
label-volume  = %{A3:pavucontrol & disown:}%percentage:2%% %{A}
format-volume = <ramp-volume> <label-volume>
label-muted   = "  muted "
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-3 = 
label-muted-foreground = #888
ramp-volume-0-foreground = ${colors.trim}
ramp-volume-1-foreground = ${colors.blue}
ramp-volume-2-foreground = ${colors.moderate}
ramp-volume-3-foreground = ${colors.urgent}


;;;;;;;;;;;;;
;; Battery ;;
;;;;;;;;;;;;;
[module/battery]
type = internal/battery
; Use $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = AC
full-at = 99
poll-interval = 5
time-format = %H:%M

format-charging    = <animation-charging> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>
format-full        = <ramp-capacity> <label-full>

label-charging = %{A1:xfce4-power-manager-settings & disown:}%percentage%% %{A}
label-discharging = %{A1:xfce4-power-manager-settings & disown:}%percentage%% %{A}
label-full = %{A1:xfce4-power-manager-settings & disown:}%percentage%% %{A}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-0-foreground = ${colors.urgent}
ramp-capacity-1-foreground = ${colors.moderate}
ramp-capacity-foreground   = ${colors.blue}

bar-capacity-width = 10
animation-charging-0 = " "
animation-charging-1 = " "
animation-charging-2 = " "
animation-charging-3 = " "
animation-charging-4 = " "
animation-charging-framerate = 750


;;;;;;;;;;
;; Menu ;;
;;;;;;;;;;
[module/menu]
type = custom/menu
format-spacing = 1
label-open = "    "
label-close = "    "
label-close-foreground = ${colors.urgent}
label-separator = " | "
# Top level
menu-0-0 = "  "
menu-0-0-exec = menu-open-1
menu-0-1 = "  "
menu-0-1-exec = menu-open-2
menu-0-2 = "    |"
menu-0-2-exec = menu-open-3
# 1
menu-1-0 = "  "
menu-1-0-exec = menu-open-0
menu-1-0-foreground = ${colors.urgent}
menu-1-1 = "  "
menu-1-1-exec = exo-open --launch WebBrowser
menu-1-2 = "  "
menu-1-2-exec = exo-open --launch TerminalEmulator
menu-1-3 = "    |"
menu-1-3-exec = exo-open --launch FileManager
# 2
menu-2-0 = "  "
menu-2-0-exec = menu-open-0
menu-2-0-foreground = ${colors.urgent}
menu-2-1 = "  "
menu-2-1-exec = xfce4-settings-manager &
menu-2-2 = "    |"
menu-2-2-exec = pavucontrol &
# 3
menu-3-0 = "  "
menu-3-0-exec = menu-open-0
menu-3-0-foreground = ${colors.urgent}
menu-3-1 = "   "
menu-3-1-exec = i3lock-blur &
menu-3-2 = "   "
menu-3-2-exec = systemctl -i reboot
menu-3-3 = "      |"
menu-3-3-exec = systemctl -i poweroff


;;;;;;;;;;;;;;;
;; Keyboard ;;;
;;;;;;;;;;;;;;;
[module/keyboard]
type           = internal/xkeyboard
blacklist-0    = scroll lock
format         = <label-layout> <label-indicator>
format-spacing = 0
label-layout   =  %layout%
label-indicator = %name%
label-indicator-padding = 2
label-indicator-background = ${colors.purple}


;;;;;;;;;;;;;;
;; Ethernet ;;
;;;;;;;;;;;;;;
[module/wired-network]
type = internal/network
interface = enp0s3
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-connected =  %local_ip%
format-connected-foreground = ${colors.purple}


;;;;;;;;;;;;;;;;
;; Filesystem ;;
;;;;;;;;;;;;;;;;
[module/filesystem]
type = internal/fs
format-mounted = <label-mounted>
label-mounted = %mountpoint% : %percentage_free%%
mount-0 = /
interval = 1200
fixed-values = false
spacing = 2
label-mounted-foreground = ${colors.moderate}


;;;;;;;;;;;;;
;; Weather ;;
;;;;;;;;;;;;;
[module/weather]
; Note, for this to work install 'python-requests'
type = custom/script
interval = 10
format = <label>
format-prefix = " "
format-prefix-foreground = #3EC13F
format-underline = #3EC13F
format-foreground = ${colors.foreground}
format-background = ${colors.background}
exec = python -u $HOME/.config/polybar/scripts/weather.py
tail = true


;;;;;;;;;;;;;
;; Spotify ;;
;;;;;;;;;;;;;
[module/spotify]
;https://github.com/NicholasFeldman/dotfiles/blob/master/polybar/.config/polybar/spotify.sh
type = custom/script
exec = $HOME/.config/polybar/scripts/spotify.sh
interval = 1
;format = <label>
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-padding = 2
format-underline = #0f0
format-prefix = "  "
format-prefix-foreground = #0f0
label = %output:0:150%


;;;;;;;;;;
;; Wifi ;;
;;;;;;;;;;
[module/wireless-network]
type = internal/network
interface =

format-connected = <ramp-signal>  <label-connected>
format-packetloss = <animation-packetloss>
label-connected = %local_ip%
ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-5 = 
ramp-signal-0-foreground = ${colors.urgent}
ramp-signal-1-foreground = ${colors.purple}
ramp-signal-2-foreground = ${colors.moderate}
ramp-signal-3-foreground = ${colors.moderate}
ramp-signal-4-foreground = ${colors.blue}
ramp-signal-5-foreground = ${colors.blue}
animation-packetloss-0 = ⚠
animation-packetloss-1 = !
animation-packetloss-0-foreground = ${colors.moderate}
animation-packetloss-1-foreground = ${colors.urgent}
animation-packetloss-framerate = 500


;;;;;;;;;;;;;;;;
;; Volume bar ;;
;;;;;;;;;;;;;;;;
[module/volume-bar]
type = internal/volume
bar-volume-font = 2
bar-volume-width = 20
format-volume = <label-volume><bar-volume>
label-volume = "  "
label-muted = "   mute"
label-volume-foreground = #B6C2E7
format-muted-foreground = #E76BB4
bar-volume-foreground-0 = #92A3F7
bar-volume-foreground-1 = #92A3F7
bar-volume-foreground-2 = #6BB3E7
bar-volume-foreground-3 = #6BB3E7
bar-volume-foreground-4 = #6BB3E7
bar-volume-foreground-5 = #6BE7D8
bar-volume-foreground-6 = #6BE7D8
bar-volume-gradient = true
bar-volume-indicator = |
bar-volume-fill = •
bar-volume-empty = ·
bar-volume-empty-foreground = #666666
