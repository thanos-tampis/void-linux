#!/bin/bash

# Picom
picom -b &

# Enable numlock ( numeric keyboard ), commented out for laptop users
# numlockx &

# Polkit, required for authentication
# /usr/libexec/xfce-polkit &

# Keyring for storing saved passwords
gnome-keyring-daemon --start --components=pkcs11 &

# Restore wallpaper
nitrogen --restore &

# Required for xfce settings to work
xfsettingsd &

# Battery power
#xfce4-power-manager &

# Dunst notifications 
dunst -config ~/.config/dunst/dunstrc &

# Side panel shortcuts for file managers
xdg-user-dirs-gtk-update &

# Conky
#conky &
#conky -c ~/.config/conky/arch2.conkyrc & sleep 1

# DWM Blocks
dwmblocks &

# Tray clipboard manager  
#clipit & 
