#!/bin/bash
set -e

# Overall script with resources.
# Configuration

# Copy skel files to /etc/skel for new users
sudo rsync -av config-files/config-files/skel/ /etc/skel/ && 
sudo chmod -R 644 /etc/skel/ &&

# Copy .bashrc, .bash, so root has the same aliases 
sudo rsync -av /etc/skel/.bashrc /root/ &&
sudo rsync -av /etc/skel/.bash /root/ &&
sudo chown -R root:root /root/.bashrc /root/.bash
sudo chmod -R 600 /root/.bashrc /root/.bash

# Copy skel files to home directory for current user
rsync -av config-files/config-files/skel/ ~/
chmod -R 755 ~/
