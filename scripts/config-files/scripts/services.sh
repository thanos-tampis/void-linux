#!/bin/bash
set -e

# Enable the Dbus service
sudo ln -s /etc/sv/dbus /var/service/dbus

# Disable dhcpcd and wpa_supplicant services:
sudo rm -fr /var/service/dhcpcd
sudo rm -fr /var/service/wpa_supplicant

# Enable NetworkManager
sudo ln -s /etc/sv/NetworkManager /var/service/NetworkManager

# Enable Bluetooth
sudo ln -s /etc/sv/bluetoothd /var/service

# Enable the UFW service
sudo ln -s /etc/sv/ufw /var/service/ufw
