#!/bin/bash
set -e

# Overall script with resources.
# Configuration

# Copy Dunst files:
# cp -R /etc/dunst ~/.config/

# Add current user to the `network` group:
sudo gpasswd -a $USER network

# Add current user to the `bluetooth` group:
sudo gpasswd -a $USER bluetooth

# Create sudoers file and give current user priviledges to shutdown, reboot system:
CURRENT_USER=$(echo $USER)
sudo echo "$CURRENT_USER ALL=(ALL) NOPASSWD: /sbin/reboot, /sbin/shutdown" | sudo tee -a /etc/sudoers.d/$USER

# Fix TV desktop app path with current user
CURRENT_USER=$(echo $USER)
sed -i "s/CURRENT_USER/$CURRENT_USER/g" /home/$CURRENT_USER/.local/share/applications/tv.desktop
mkdir /home/$CURRENT_USER/.config/tv

# Changes shell Symlink
# sudo ln -sf bash /bin/sh
sudo xbps-alternatives -s bash

# View new Shell Symlink
ls -l /bin/sh

# Stop getting dbus error:
sudo dbus-uuidgen | sudo tee > sudo /var/lib/dbus/machine-id

# Customize font display in the graphical session and Fix Font Rendering
sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
sudo xbps-reconfigure -f fontconfig
