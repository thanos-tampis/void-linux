#!/bin/bash
set -e

# Overall script with resources.
# Configuration

# Copy skel files
bash config-files/scripts/skel-files.sh

# UFW
bash config-files/scripts/ufw.sh

# Misc
bash config-files/scripts/misc.sh

# Default configuration
bash config-files/scripts/default-configuration.sh

# Services
bash config-files/scripts/services.sh
