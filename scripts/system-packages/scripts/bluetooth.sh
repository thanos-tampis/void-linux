#!/bin/bash
set -e

echo "##############    Install Bluetooth   ##############"

# base bluetooth packages
bluetoothBasePackages='                    
                    bluez
                    bluez-alsa
                    libspa-bluetooth
                    ldacBT
                    blueman
				'

for package in $bluetoothBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi
	
done

echo "################################################################"
echo "######    Bluetooth installed successfully   #######"
echo "################################################################"
