#!/bin/bash
set -e

echo "##############    Install sound stuff   ##############"

# base sound packages
soundBasePackages='
				pulseaudio 
				alsa-plugins-pulseaudio 
				pavucontrol
				pamixer
				alsa-utils 
				alsa-plugins 
				alsa-lib 
				alsa-firmware
				gst-plugins-base1
				gst-plugins-good1 
				gst-plugins-bad1 
				gst-plugins-ugly1
				gstreamer1
				pasystray
				volume_key
				libmad
				'

for package in $soundBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi	
	
done

echo "################################################################"
echo "##########   Sound stuff installed successfully   ##############"
echo "################################################################"
