#!/bin/bash
set -e

echo "##############    Install various fonts   ##############"

# base font packages
fontBasePackages='
				ttf-opensans
				dejavu-fonts-ttf
				liberation-fonts-ttf
				noto-fonts-ttf
				font-awesome
                font-awesome6		
				'

for package in $fontBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi
	
done

echo "################################################################"
echo "##############    Fonts installed successfully   ##############"
echo "################################################################"
