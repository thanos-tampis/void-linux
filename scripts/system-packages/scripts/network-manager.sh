#!/bin/bash
set -e

echo "##############    Install and enable Network Manager stuff   ##############"

# base network packages
networkBasePackages='
				NetworkManager
				NetworkManager-openvpn
				network-manager-applet
                inetutils-ifconfig
				'

for package in $networkBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi
	
done

echo "################################################################"
echo "######    Network Manager stuff installed successfully   #######"
echo "################################################################"
