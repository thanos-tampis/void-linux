#!/bin/bash
set -e

echo "##############    Install various themes and icon-themes   ##############"

# base theme packages
themeBasePackages='		
				arc-theme
				numix-themes
				paper-gtk-theme							
				'

for package in $themeBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi	
		
done


# base icon theme packages
iconThemesBasePackages='
				paper-icon-theme			
				papirus-icon-theme				
				'

for package in $iconThemesBasePackages
do 
	
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi
		
done

echo "################################################################"
echo "##############    Various themes installed successfully   ##############"
echo "################################################################"
