#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Install various fonts
bash system-packages/scripts/fonts.sh

# Install various themes
bash system-packages/scripts/themes.sh

# Network Manager
bash system-packages/scripts/network-manager.sh

# Sound stuff
bash system-packages/scripts/sound.sh

# Bluetooth
bash system-packages/scripts/bluetooth.sh
