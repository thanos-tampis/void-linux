#!/bin/bash
set -e

echo
echo " Don't run this as root "
echo

# Initial Core setup
function initial_setup() {
    bash core-install/setup.sh
}

# System base packages 
function system_packages() {
    bash system-packages/setup.sh
}

# Extra packages 
function extra_packages() {
    bash extra-packages/setup.sh
}

# Config files setup 
function config_setup() {
    bash config-files/setup.sh
}

# Enable WM 
function wm() {
    bash wm/setup.sh
}

# Enable LightDM 
function enable_lightdm() {
    bash config-files/scripts/lightdm.sh
}

function run_menu() {

    COLUMNS=12

    echo "******************************************************************"
    PS3="Enter a number of your choice (1-7):" 

    options=(
        "Initial Core setup"
        "System base packages"
        "Extra packages"
        "Config files setup"
        "Choose a WM to install"
        "Enable LightDM"
    )

    select answer in "${options[@]}" "Quit"; do

        case "$REPLY" in
            1)
                initial_setup
                run_menu
                ;;
            2)
                system_packages
                run_menu
                ;;
            3)
                extra_packages
                run_menu
                ;;
            4)
                config_setup
                run_menu
                ;;  
            5)
                wm
                run_menu
                ;;
            6)
                enable_lightdm
                run_menu
                ;;                                
            "Quit")
                break
                ;;
            $(( ${#options[@]}+1 )) ) echo "Goodbye!"; exit;;
            *) echo "Invalid option. Try another one.";continue;;
        esac

    done

}

run_menu