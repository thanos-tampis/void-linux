# Void Openbox/DWM

Void Openbox/DWM Auto-Rice Bootstrapping Scripts.

Script that auto installs and configuring a fully-functioning and minimal Openbox and DWM Void Linux environment.

## Running script: 

You must have base Void installed first.   
Once you have Void running run **'bash setup.sh'** and follow the on-screen instructions.

#### Important note:

Please use the **glibc (GNU C Library)** as there will be nonfree packages installed that are not available with musl.

## After Void Base Installation:

If you have installed Void Base only and after the installation you don't have Internet access, you can use *wpa_supplicant* to connect:

#### Example Properties:

```
SSID (Wifi Name): vodafoneA4ACB9
Password: This!isMyStrongPa$$W0rd
```

1. Login as root:

    *   `sudo -s`

2. You must then create the correct configuration using wpa_passphrase (do not confused wpa_passphrase and wpa_supplicant):

    *   `wpa_passphrase vodafoneA4ACB9 This!isMyStrongPa$$W0rd >> /etc/wpa_supplicant/wpa_supplicant.conf`

3. Find your device name (like wlp3s0), by executing:

    *   `ip link show`

4. Start wpa-supplicant, but change wlp3s0 to your device name:

    *   `wpa_supplicant -B -i wlp3s0 -c /etc/wpa_supplicant/wpa_supplicant.conf`

5. link service to runit:

    *   `ln -s /etc/sv/wpa_supplicant /var/service`

6. Test your connection

    *   `ping google.com`

When you finish setup, or if you have installed NetworkManager (you can use nmtui), remove wpa_supplicant service created:

*   `sudo ln -s /etc/sv/wpa_supplicant /var/service`

## Post configuration:

- You may want to run *obmenu-generator -p* and edit files to replace Openbox default applications such as xterm and geany.
- Remove various Keyboard shortcuts as they conflict with the custom Openbox uses
- Set default applications from Openbox menu
- Firefox minimal theme that works with both Light/Dark Themes: https://github.com/crambaud/waterfall


## Known Issues:

### If gvfs is not available

You may need to use *"dbus-run-session"* to start your desktop session:

Please edit your .xinitrc file so instead of:

```
exec dwm;;
exec openbox-session;;
```

These should be:

```
exec dbus-run-session dwm;;
exec dbus-run-session openbox-session;;
```

Of if you use something like lightdm:

Edit Xsession in /etc/lightdm and replace the last line *"exec $@"* to *"exec dbus-run-session $@"*:

`sudo nvim /etc/lightdm/Xsession`

### Screen Teating 

Screen Tearing is a common issue.

** For Intel **

Please follow guide here: 

https://wiki.archlinux.org/title/intel_graphics#With_the_Intel_driver

** For Nvidia **

Please follow guide here: 

https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Avoid_screen_tearing
