#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Update Packages
bash core-install/scripts/update-xbps.sh

# Install mirrors
bash core-install/scripts/mirrors.sh

# Install xorg stuff
bash core-install/scripts/xorg.sh

# Install Lightdm
bash core-install/scripts/lightdm.sh

# Install Openbox/DWM
bash core-install/scripts/install-core.sh
