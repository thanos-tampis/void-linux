#!/bin/bash
set -e

# Run the update 3 times

echo "#######	Synchronize repo databases and Update System	#######"

sudo xbps-install -Su

sudo xbps-install -Su

sudo xbps-install -Su

echo "################################################################"
echo "##############	System updated succesfully	##############"
echo "################################################################"

