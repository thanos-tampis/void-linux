#!/bin/bash
set -e

echo "##############    Xserver setup and graphics   ##############"

xorgBasePackages='
                xorg
                xorg-server 
                xorg-apps                 
                xinit
                '

for package in $xorgBasePackages
do
 
    #check if package is already installed
    if xbps-query $package &> /dev/null; then

        echo "################################################################"
        echo "################## "$package" already installed"
        echo "################################################################"

    else

        sudo xbps-install -S $package -y

        echo "################################################################"
        echo "################## "$package" installed successfully"
        echo "################################################################"

    fi
    
done

# Choose Nvidia or Intel graphics:

# Intel Graphics
function intel_graphics() {

    #check if Intel graphics package is already installed
    if xbps-query linux-firmware-intel mesa-dri intel-video-accel &> /dev/null; then

        echo "################################################################"
        echo "################## Intel graphics already installed"
        echo "################################################################"

    else

        sudo xbps-install -S linux-firmware-intel -y

        echo "################################################################"
        echo "################## Intel graphics installed successfully"
        echo "################################################################"

    fi
    echo "Intel graphics installed successfully"
    run_graphics_menu

}

# Nvidia Graphics 
function nvidia_graphics() {

    
    #check if Nvidia graphics package is already installed
    if xbps-query xf86-video-nouveau &> /dev/null; then

        echo "################################################################"
        echo "################## Nvidia graphics already installed"
        echo "################################################################"

    else

        sudo xbps-install -S xf86-video-nouveau -y

        echo "################################################################"
        echo "################## Nvidia graphics installed successfully"
        echo "################################################################"

    fi    
    echo "Nvidia graphics installed successfully"
    run_graphics_menu

}

function run_graphics_menu() {

    COLUMNS=12

    echo "******************************************************************"
    PS3="Enter a number of your choice (1-3):" 

    options=(
        "Intel graphics" 
        "Nvidia graphics"
    )

    select answer in "${options[@]}" "Quit - No need to install graphics"; do

        case "$REPLY" in
            1)
                intel_graphics                
                ;;
            2)
                nvidia_graphics                
                ;;                                       
            "Quit - No need to install graphics")
                break
                ;;
            $(( ${#options[@]}+1 )) ) echo "Goodbye!"; exit;;
            *) echo "Invalid option. Try another one.";continue;;
        esac

    done

}

run_graphics_menu


echo "################################################################"
echo "######    Xserver and graphics installed successfully    #######"
echo "################################################################"

