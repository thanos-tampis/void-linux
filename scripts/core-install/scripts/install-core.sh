#!/bin/bash
set -e

echo "##############    Install Core Packages   ##############"

# base packages
coreBasePackages='
				bash-completion
				xfce4-settings
				xfce4-power-manager
				xfce4-notifyd
				xdg-user-dirs
				xdg-user-dirs-gtk
				gtk+
				gtk+3
				gsimplecal-gtk3
				libnotify
				dunst
				numlockx
				xdotool
				picom
				arandr
				libXrandr
				nitrogen
				lxinput
				playerctl
				brightnessctl
				tint2	
				dbus
				elogind
				rtkit				
				'

for package in $coreBasePackages
do 
	
	#check if package is already installed
	if xbps-query $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		sudo xbps-install -S $package -y

		echo "################################################################"
		echo "################## "$package" installed successfully"
		echo "################################################################"

	fi
	
done
