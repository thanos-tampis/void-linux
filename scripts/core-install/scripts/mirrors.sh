#!/bin/bash
set -e

echo "##############	Assign closest mirror (Tier 1)	##############"

# Assign mirror:
sudo echo "repository=https://alpha.de.repo.voidlinux.org/current" | sudo tee -a /etc/xbps.d/00-repository-main.conf 
sudo chmod 644 /etc/xbps.d/00-repository-main.conf

echo "##############	Install nonfree repository	##############"

#check if Nonfree repo is already installed
if xbps-query void-repo-nonfree &> /dev/null; then

	echo "################################################################"
	echo "################## nonfree repo already installed"
	echo "################################################################"

else

	sudo xbps-install -S void-repo-nonfree -y

	echo "################################################################"
	echo "################## nonfree repo installed successfully"
	echo "################################################################"

fi

# Assign mirror
sudo echo "repository=https://alpha.de.repo.voidlinux.org/current/nonfree" | sudo tee -a /etc/xbps.d/10-repository-nonfree.conf 
sudo chmod 644 /etc/xbps.d/10-repository-nonfree.conf

echo "##############	Synchronize repo databases and Update System	##############"

sudo xbps-install -S
sudo xbps-install -Su

echo "################################################################"
echo "#########		Mirror assigned and updates are done	##########"
echo "################################################################"

