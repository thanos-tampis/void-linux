#!/bin/bash
set -e

echo "##############    Install Lightdm   ##############"

# base lightdm packages
lightdmBasePackages='
				lightdm 
				lightdm-gtk3-greeter 
				lightdm-gtk-greeter-settings
				'

for package in $lightdmBasePackages
do
 
	#check if package is already installed
	if xbps-query $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		sudo xbps-install -S $package -y

		echo "################################################################"
		echo "################## "$package" installed successfully"
		echo "################################################################"

	fi
	
done

echo "################################################################"
echo "##############    Lightdm installed successfully  ##############"
echo "################################################################"
