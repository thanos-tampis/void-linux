#!/bin/bash
set -e

# Create DWM menu entry for Window Managers.
sudo mkdir /usr/share/xsessions
sudo touch /usr/share/xsessions/dwm.desktop
cat config-files/config-files/suckless/dwm.desktop | sudo tee -a /usr/share/xsessions/dwm.desktop

# Install packages required to build
sudo xbps-install -S base-devel libX11-devel libXft-devel libXinerama-devel -y 

# Install font to fix crashes due to emojis in the title bar
# sudo xbps-install -S font-symbola -y

# Install DWM
cd ~/.config/suckless/dwm
sudo make clean install

echo "#########"
echo "#########  DWM Installed  ################"
echo "#########"

# Install dwmblocks-async
cd ~/.config/suckless/dwmblocks-async
sudo make clean install

echo "#########"
echo "#########  Dwmblocks Installed  ################"
echo "#########"

# Install ST terminal
cd ~/.config/suckless/st
sudo make clean install

echo "#########"
echo "#########  ST terminal Installed  ################"
echo "#########"