#!/bin/bash
set -e

# Install Openbox
coreBasePackages='
				openbox 
				obconf 
				obmenu-generator
				'

for package in $coreBasePackages
do 
	
	#check if package is already installed
	if xbps-query $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		sudo xbps-install -S $package -y

		echo "################################################################"
		echo "################## "$package" installed successfully"
		echo "################################################################"

	fi
	
done