#!/bin/bash
set -e

# Choose DWM or Openbox Menu:


# DWM
function dwm_wm() {
   
	# Install Suckless stuff
	bash wm/scripts/suckless.sh

    echo "DWM installed successfully"
    wm_menu

}

# Openbox 
function openbox_wm() {

	# Install Openbox
	bash wm/scripts/openbox.sh

    echo "Openbox installed successfully"
    wm_menu

}

function wm_menu() {

    COLUMNS=12

    echo "******************************************************************"
    echo "******************************************************************"    
    PS3="Enter choice to install DWM or Openbox (1-3):" 

    options=(
        "DWM" 
        "Openbox"
    )

    select answer in "${options[@]}" "Quit - No need to install any wm"; do

        case "$REPLY" in
            1)
                dwm_wm
                ;;
            2)
                openbox_wm
                ;;                                       
            "Quit - No need to install any wm")
                break
                ;;
            $(( ${#options[@]}+1 )) ) echo "Goodbye!"; exit;;
            *) echo "Invalid option. Try another one.";continue;;
        esac

    done

}

wm_menu
