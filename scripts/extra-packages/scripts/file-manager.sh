#!/bin/bash
set -e

echo "################################################################"
echo "#########   Install Thunar Manager  ################"
echo "################################################################"

# base font packages
fileManagerBasePackages='
				Thunar
				thunar-archive-plugin
				thunar-media-tags-plugin
				thunar-volman
				tumbler
				ffmpegthumbnailer
				gvfs
				gvfs-mtp
				gvfs-afc
				gvfs-smb
				nfs-utils
				file-roller 
				fsarchiver 
				udiskie
				udisks2									
				'

for package in $fileManagerBasePackages
do 
	
	#check if package is already installed
	if xbps-query $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		sudo xbps-install -S $package -y

		echo "################################################################"
		echo "################## "$package" installed successfully"
		echo "################################################################"

	fi
	
done


echo "################################################################"
echo "#########  Thunar File Manager Installed  ################"
echo "################################################################"
