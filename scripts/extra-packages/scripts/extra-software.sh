#!/bin/bash
set -e

# base extra software packages
extraSoftwareBasePackages='
				ufw
				xcape
				desktop-file-utils
				polkit
				xfce-polkit
				gnome-keyring
				libgnome-keyring
				gnome-screenshot
				sound-theme-freedesktop
				gpicview
				ranger
				scrot
				i3lock
				startup-notification				
				ntfs-3g
				git
				p7zip
				unzip
				rofi
				htop
				openssh
				mpv
				conky
				wget
				dmidecode
				hardinfo
				curl
				rsync
				screen
				unrar
				tar
				net-tools
				evince
				firefox
				zenity
				filezilla
				galculator-gtk3
				neovim
				xclip
				jq
				rxvt-unicode
				urxvt-perls						
				ImageMagick
				gimp
				darktable
				caffeine-ng
				clipit
				skippy-xd
				vscode
				deadbeef
				polybar				
				'

for package in $extraSoftwareBasePackages
do 
	
	#check if package is already installed
	if xbps-query $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		sudo xbps-install -S $package -y

		echo "################################################################"
		echo "################## "$package" installed successfully"
		echo "################################################################"

	fi
		
done

echo "################################################################"
echo "################    extra software installed  ##################"
echo "################################################################"
