#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Install File Manager
bash extra-packages/scripts/file-manager.sh

# Extra packages
bash extra-packages/scripts/extra-software.sh
